# TestWeatherForecast

App is live on https://web-app-weather-forecast.herokuapp.com

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Application description:

1. User can enter a city or multiple cities separated by comma end search for weather results. If input field is empty there is a validation message.
If input field doesn't contain comma or city contains number, there is a validation message. If input field contains city/cities with all capital letters
there is a custom validation message. If user enters invalid city, there is toastr error message. If user duplicates cities there is also toastr error message;
2. Results are displayed on the new page. User can enter as much cities as he wants. If user enters 5 or more cities, results page displays city names dropdown
in the navigation menu. On this page user can remove city tags from the navigation menu and list below, one by one or all by one click on the Clear All button.
Clicking on the city weather forecast box from the result list navigates to a page with a more detailed view of the current weather conditions, as well as a 5 
day forecast in every 3 hours;
3. On details page there is a weather table with current weather conditions, Google map centered on the chosen city and collapsible title for 5 days weather 
forecast. If user clicks on the collapsible title, he gets unfolded list of 5 days weather forecast. Clicking anywhere on the body of the list, folds list into 
visible title;


Technologies and dependencies:
Angular - 6.0.6;
Typescript - 2.7.2;
Angular Material - 6.4.2;
Sass;
HTML5;
CSS3;
Linter - 5.9.1;
Dependency management (npm);



## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
