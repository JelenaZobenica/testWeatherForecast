import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Weather} from '../models/weather.model';
import {Subject} from 'rxjs';
import {BehaviorSubject, Observable} from 'rxjs/Rx';
import {catchError} from 'rxjs/internal/operators';
import {map} from 'rxjs/operators';

let WEATHER_ITEMS: Weather[] = [];
let CITY_ITEMS: string[] = [];

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  searchInput: BehaviorSubject<string>;
  isRemoved: Subject<boolean>;
  removeCities: Subject<Weather>;
  isClickedOutside: Subject<boolean>;
  isFadeIn: Subject<boolean>;
  isSorted: Subject<boolean>;
  isOpenModal: Subject<boolean>;

  constructor(private http: HttpClient) {
    this.searchInput = new BehaviorSubject<string>('');
    this.isRemoved = new Subject();
    this.removeCities = new Subject();
    this.isClickedOutside = new Subject();
    this.isFadeIn = new Subject();
    this.isSorted = new Subject();
    this.isOpenModal = new Subject();
  }

  getWetherItems() {
    return WEATHER_ITEMS;
  }

  addWeatherItem(WeatherItem: Weather) {
    WEATHER_ITEMS.push(WeatherItem);
  }

  resetWeatherItems() {
    WEATHER_ITEMS = [];
  }

  getWeatherByCityName(cityName: string) {
    const url = `http://api.openweathermap.org/data/2.5/weather?q=${cityName}&APPID=fbc943265329af1ac2025cb8c82356b7&units=metric`;
    return this.http.get<Weather>(url)
      .pipe(
        map(response => new Weather(response)),
        catchError(this.handleError<Weather>('getWeather'))
      );
  }

  getWeatherByCityName1(cityName: string): Observable<any> {
    const url = `http://api.openweathermap.org/data/2.5/weather?q=${cityName}&APPID=fbc943265329af1ac2025cb8c82356b7&units=metric`;
    return this.http.get(url)
      .pipe(
        map(response => new Weather(response))
      );
  }

  getFiveDayWeatherByCityName(cityName: string) {
    const url = `http://api.openweathermap.org/data/2.5/forecast?q=${cityName}&APPID=fbc943265329af1ac2025cb8c82356b7&units=metric`;
    return this.http.get(url)
      .map(response => {
        return response['list'].map(weather => new Weather(weather));
      })
      .catch(this.handleError<Weather>('getFiveDayWeather'));
  }

  getSearchInput(input: string) {
    this.searchInput.next(input);
  }

  removeAllCitiesFromNavBar(removeAll: boolean) {
    this.isRemoved.next(removeAll);
  }

  removeCity(cityName: string) {
    const weather = WEATHER_ITEMS.filter(w => w.cityName.toLowerCase() === cityName.toLowerCase())[0];
    this.removeCities.next(weather);
    WEATHER_ITEMS = WEATHER_ITEMS.filter(w => w !== weather);
  }

  clickOutside(isClicked: boolean) {
    this.isClickedOutside.next(isClicked);
  }

  sortClicked(isFade: boolean, isSort?: boolean) {
    this.isFadeIn.next(isFade);
    this.isSorted.next(isSort);
  }

  addCity(cityName: string) {
    CITY_ITEMS.push(cityName);
  }

  removeCityItems(cityName: string) {
    CITY_ITEMS = CITY_ITEMS.filter(city => city !== cityName);
  }

  getCityItems() {
    return CITY_ITEMS;
  }

  resetCityItems() {
    CITY_ITEMS = [];
  }

  openModal(isOpen) {
    this.isOpenModal.next(isOpen);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(`Error; ${error.status} ${error.statusText}`);
      return Observable.of(result as T);
    };
  }
}
