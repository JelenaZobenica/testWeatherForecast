import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {SearchCityModule} from './components/search-city/search-city.module';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {appRoutes} from './routes';
import {SearchResultsModule} from './components/search-results/search-results.module';
import {CityWeatherDetailsModule} from './components/city-wather-details/city-weather-details.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {DialogBoxModule} from './components/dialog-box/dialog-box.module';
import {DialogBoxComponent} from './components/dialog-box/dialog-box.component';
import {WelcomeModule} from './components/welcome/welcome.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  entryComponents: [
    DialogBoxComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    WelcomeModule,
    SearchCityModule,
    SearchResultsModule,
    CityWeatherDetailsModule,
    BrowserAnimationsModule,
    DialogBoxModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      preventDuplicates: true,
      easeTime: 500
    }),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
