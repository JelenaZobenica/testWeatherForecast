import {Routes} from '@angular/router';
import {SearchCityComponent} from './components/search-city/search-city.component';
import {SearchResultsComponent} from './components/search-results/search-results.component';
import {CityWeatherDetailsComponent} from './components/city-wather-details/city-weather-details.component';
import {SearchResultsResolver} from './components/search-results/search-results.resolver';
import {CityWeatherDetailsResolver} from './components/city-wather-details/city-weather-details.resolver';
import {WelcomeComponent} from "./components/welcome/welcome.component";

export const appRoutes: Routes = [

  {path: 'welcome', component: WelcomeComponent},
  {path: 'search_city', component: SearchCityComponent},
  {
    path: 'city_weather/:name/details',
    component: CityWeatherDetailsComponent,
    resolve: {weather: CityWeatherDetailsResolver}
  },
  {
    path: 'search_results',
    component: SearchResultsComponent,
    resolve: {weathers: SearchResultsResolver}
  },
  {path: '**', redirectTo: 'welcome', pathMatch: 'full'}
];
