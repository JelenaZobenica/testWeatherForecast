import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'convertMeasure'})
export class ConvertMeasure implements PipeTransform {

  transform(value: number): string {
    if (!value) {
      return;
    }

    const convertMeterPerSecondToKilometerPerHour: number = (value * 18) / 5;

    return convertMeterPerSecondToKilometerPerHour.toFixed(1);
  }
}
