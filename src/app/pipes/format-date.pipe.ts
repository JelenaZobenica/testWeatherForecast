import {Pipe, PipeTransform} from '@angular/core';
import * as moment from "moment";

@Pipe({name: 'formatDate'})
export class FormatDate implements PipeTransform {

  transform(value: any): any {
    if (!value) {
      return;
    }

    const date = moment.unix(value).format('dddd').substring(0, 3) + ' ' + moment.unix(value).format('MMM Do, h:mm a');

    return date;
  }
}
