import {Pipe, PipeTransform} from "@angular/core";

@Pipe({name: 'firstLetterUpercase'})
export class FirstLetterUpercase implements PipeTransform {

  transform(value: string): string {
    if (!value) {
      return;
    }
    const inputWord = value.trim();
    const outputWord = inputWord.charAt(0).toUpperCase() + inputWord.slice(1).toLowerCase();

    return outputWord;
  }
}
