import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'filter',
  // pure: false => pipe se pokrece svaki put kad se na stranici promene value(npr. dodamo nesto u listu koju filtiramo)
  // koje pipe prima kao argument metode transform. Po default-u pure je true.
})
export class FilterPipe implements PipeTransform {
  transform(value: any, filterValue: string, propertyName?: string) {
    if (value.length === 0 || !filterValue) { return value; }
    const resultArray = [];

    for (const item of value) {
      // case if value array has key:value pairs
      if (propertyName) {
        if (item[propertyName].toLowerCase().indexOf(filterValue.toLowerCase()) !== -1) {
          resultArray.push(item);
        }
      } else {
        if (item.toLowerCase().indexOf(filterValue.toLowerCase()) !== -1) {
          resultArray.push(item);
        }
      }
    }
    return resultArray;
  }
}
