import {NgModule} from '@angular/core';
import {ConvertMeasure} from './convert-measure.pipe';
import {FilterPipe} from './filter.pipe';

@NgModule({
  declarations: [
    ConvertMeasure,
    FilterPipe
  ],
  imports: [],
  exports: [ConvertMeasure, FilterPipe]
})
export class PipesModule {}
