import {NgModule} from '@angular/core';
import {AutofocusDirective} from './autofocus.directive';

@NgModule({
  declarations: [AutofocusDirective],
  imports: [],
  exports: [AutofocusDirective]
})
export class DirectiveModule {}
