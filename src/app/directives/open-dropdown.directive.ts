import {Directive, EventEmitter, HostBinding, HostListener, Output} from '@angular/core';

@Directive({
  selector: '[open-dropdown]'
})
export class OpenDropdownDirective {
  @HostBinding('class.open') isOpen = false;
  @Output() isOpenChange: EventEmitter<boolean> = new EventEmitter();

  @HostListener('click', ['$event']) toggleOpen(event: MouseEvent) {
    this.isOpen =  !this.isOpen;
    this.isOpenChange.emit(this.isOpen);
    event.stopPropagation();
  }
}
