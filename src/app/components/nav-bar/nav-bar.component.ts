import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Weather} from '../../models/weather.model';

@Component({
  selector: 'nav-bar',
  templateUrl: './navbar.component.html',
  styleUrls: ['../../../styles/navbar/navbar.scss']
})
export class NavbarComponent implements OnInit {

  @Input() weathers: Weather[];
  currentUrl: string;

  constructor(private router: Router) {}

  ngOnInit() {
    this.currentUrl = this.router.url;
  }
}
