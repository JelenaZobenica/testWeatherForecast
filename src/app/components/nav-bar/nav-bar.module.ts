import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import {RouterModule} from '@angular/router';
import {NavbarComponent} from './nav-bar.component';
import {SearchResultsNavBarModule} from "../search-results-nav-bar/search-results-nav-bar.module";

@NgModule({
  declarations: [
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule,
    SearchResultsNavBarModule
  ],
  providers: [],
  exports: [NavbarComponent]
})
export class NavbarModule { }
