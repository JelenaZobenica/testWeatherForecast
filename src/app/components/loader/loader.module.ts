import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import {RouterModule} from '@angular/router';
import {LoaderComponent} from './loader.component';

@NgModule({
  declarations: [
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule
  ],
  providers: [],
  exports: [LoaderComponent]
})
export class LoaderModule { }
