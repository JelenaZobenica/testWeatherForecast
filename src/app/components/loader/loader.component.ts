import {Component} from '@angular/core';

@Component({
  selector: 'loader',
  templateUrl: './loader-spinner.component.html',
  styleUrls: ['../../../styles/loader/loader-spinner.scss']
})
export class LoaderComponent {
}
