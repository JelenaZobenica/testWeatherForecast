import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {SearchResultsComponent} from './search-results.component';
import {CityWeatherModule} from '../city-weather/city-weather.module';
import {SearchResultsNavBarModule} from '../search-results-nav-bar/search-results-nav-bar.module';
import {NavbarModule} from '../nav-bar/nav-bar.module';
import {MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatTooltipModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {LoaderModule} from '../loader/loader.module';
import {MapCitiesModule} from '../map-cities/map-cities.module';
import {PipesModule} from '../../pipes/pipe.module';
import {ModalModule} from '../modal/modal.module';

@NgModule({
  declarations: [
    SearchResultsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule,
    CityWeatherModule,
    SearchResultsNavBarModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatTooltipModule,
    NavbarModule,
    LoaderModule,
    MapCitiesModule,
    PipesModule,
    ModalModule
  ],
  providers: [],
  exports: [SearchResultsComponent]
})
export class SearchResultsModule { }
