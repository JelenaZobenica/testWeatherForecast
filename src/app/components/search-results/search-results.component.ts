import {Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {WeatherService} from '../../services/weather.service';
import {Weather} from '../../models/weather.model';
import 'rxjs/add/operator/takeUntil';
import {Subject} from 'rxjs/Rx';
import {animate, group, style, transition, trigger, useAnimation} from '@angular/animations';
import {ActivatedRoute} from '@angular/router';
import {bounceOutAnimation} from '../../animations/animations';

@Component({
  selector: 'search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['../../../styles/search-results/search-results.scss'],
  animations: [
    trigger('searchResults', [
      transition(':enter', [
        group([
          style({opacity: 0, transform: 'translateY(300%)'}),
          animate('4s cubic-bezier(.56,.23,0,1.01)', style({transform: 'translateY(-1%)', opacity: 1})),
        ])
      ])
    ]),
    trigger('moveUndoButtonAnimation', [
      transition(':leave', [
        useAnimation(bounceOutAnimation)
      ])
    ])
  ]
})
export class SearchResultsComponent implements OnInit, OnDestroy {
  weathers: Weather[] = null;
  filterValue: string;
  isSort: boolean;
  sortUpTemp: boolean;
  sortDownTemp: boolean;
  sortUpName: boolean;
  sortDownName: boolean;
  isAllWeathersRemoved: boolean;
  removedWeather: Weather;
  // @ViewChild('message') messageNoCities: ElementRef;
  unsubscribe: Subject<void>;
  showModal: boolean;

  constructor(private weatherService: WeatherService,
              private renderer: Renderer2,
              private elemRef: ElementRef,
              private route: ActivatedRoute) {
    // this.messageNoCities = this.elemRef.nativeElement;
    this.unsubscribe = new Subject<void>();
  }

  ngOnInit() {
    this.showModal = false;
    this.sortUpTemp = false;
    this.sortDownTemp = false;
    this.sortUpName = false;
    this.sortDownName = false;
    this.isAllWeathersRemoved = false;
    this.route.data.forEach(data => this.weathers = data['weathers']);
    this.weatherService.isRemoved.subscribe(removedCity => this.weathers = []);

    this.weatherService.removeCities.subscribe(w => {
      this.weathers = this.weatherService.getWetherItems()
        .filter(weather => weather.cityName.toLowerCase() !== w.cityName.toLowerCase().trim());
      this.filterValue = '';
    });

    this.weatherService.isOpenModal.subscribe(isOpen => {
      this.showModal = isOpen;
    });

    this.weatherService.isSorted.subscribe(isSort => this.isSort = isSort);
    this.weatherService.isRemoved.subscribe(isRemoved => this.isAllWeathersRemoved = isRemoved);
    this.weatherService.removeCities.subscribe(cityWeather => this.removedWeather = cityWeather);
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  animationDone(event) {
    // this.renderer.addClass(this.messageNoCities.nativeElement, 'afterRemovingWeatherItems');
  }

  sortByCityNames(sortingOrder: string) {
    if (sortingOrder === 'asc') {
      this.weathers.sort(this.sortByCityNamesAsc);
      this.sortUpName = true;
      this.sortDownName = false;
    } else {
      this.weathers.sort(this.sortByCityNamesDesc);
      this.sortDownName = true;
      this.sortUpName = false;
    }
    this.weatherService.sortClicked(false, true);
    this.sortUpTemp = false;
    this.sortDownTemp = false;
  }

  private sortByCityNamesAsc(w1: Weather, w2: Weather) {
    return w1.cityName === w2.cityName ? 0 : w1.cityName < w2.cityName ? -1 : 1;
  }

  private sortByCityNamesDesc(w1: Weather, w2: Weather) {
    return w1.cityName === w2.cityName ? 0 : w1.cityName > w2.cityName ? -1 : 1;
  }

  sortByTemperature(sortingOrder: string) {
    if (sortingOrder === 'asc') {
      this.weathers.sort(this.sortByTempAsc);
      this.sortUpTemp = true;
      this.sortDownTemp = false;
    } else {
      this.weathers.sort(this.sortByTempDesc);
      this.sortDownTemp = true;
      this.sortUpTemp = false;
    }
    this.weatherService.sortClicked(false, true);
    this.sortDownName = false;
    this.sortUpName = false;
  }

  private sortByTempAsc(w1: Weather, w2: Weather) {
    return w1.temperature - w2.temperature;
  }

  private sortByTempDesc(w1: Weather, w2: Weather) {
    return w2.temperature - w1.temperature;
  }

  filterCity(filterValue: string, event) {
    if (this.isSort === true) {
      this.weatherService.sortClicked(false, true);
    } else {
      this.weatherService.sortClicked(false);
    }

    // if (filterValue || event.keyCode === 8) {
    //   this.weathers = this.weatherService.getWetherItems();
    //   this.weathers = this.filter(this.weathers, filterValue);
    // }
    //
    // if (!filterValue) {
    //   this.weathers = this.weatherService.getWetherItems();
    // }
  }

  private filter(weathers: Weather[], value: string): Weather[] {
    return weathers.filter(weather => weather.cityName.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  resetSearch() {
    this.filterValue = '';
    this.weathers = this.weatherService.getWetherItems();
  }

  undoSort() {
    this.isSort = false;
    this.sortUpTemp = false;
    this.sortDownTemp = false;
    this.sortDownName = false;
    this.sortUpName = false;
    this.weatherService.sortClicked(true, false);
  }

  closeModal(action?: string) {
    if (action === 'yes') {
      this.weathers = [];
      this.weatherService.resetWeatherItems();
      this.weatherService.resetCityItems();
      this.weatherService.removeAllCitiesFromNavBar(true);
    }
    this.showModal = false;
  }
}
