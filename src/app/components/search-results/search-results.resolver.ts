import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {WeatherService} from '../../services/weather.service';

@Injectable({
  providedIn: 'root'
})
export class SearchResultsResolver implements Resolve<any> {

  constructor(private weatherService: WeatherService) {}

  resolve(route: ActivatedRouteSnapshot): any {
    return this.weatherService.getWetherItems();
  }

}
