import {NgModule} from '@angular/core';
import {SearchCityComponent} from './search-city.component';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatKeyboardModule} from '@ngx-material-keyboard/core';
import {MatTooltipModule} from '@angular/material/tooltip';
import {DirectiveModule} from '../../directives/directive.module';

@NgModule({
  declarations: [
    SearchCityComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    MatKeyboardModule,
    MatTooltipModule,
    DirectiveModule,
    RouterModule
  ],
  providers: [],
  exports: [SearchCityComponent]
})
export class SearchCityModule {
}
