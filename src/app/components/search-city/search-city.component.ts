import {
  AfterViewInit, Component, ElementRef, HostListener, OnDestroy, OnInit, Renderer2,
  ViewChild
} from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CitynameValidators} from '../../validators/cityname.validators';
import {WeatherService} from '../../services/weather.service';
import {Subject, Subscription} from 'rxjs/Rx';
import {ToastrService} from 'ngx-toastr';
import {MatKeyboardComponent, MatKeyboardRef} from '@ngx-material-keyboard/core';
import {Weather} from '../../models/weather.model';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';

@Component({
  selector: 'search-city',
  templateUrl: './search-city.component.html',
  styleUrls: ['../../../styles/search-city/search-city.scss']
})
export class SearchCityComponent implements OnInit, OnDestroy, AfterViewInit {

  // @ViewChild('cityNameFocus') input: ElementRef;
  mouseoverSearch: boolean;
  cityForm: FormGroup;
  cityName: FormControl;
  cities: string[];
  errorNames: string;
  // unsubscribe: Subject<void>;
  isKeyboard = false;
  private enterSubscription: Subscription;
  private keyboardRef: MatKeyboardRef<MatKeyboardComponent>;

  constructor(private router: Router,
              private weatherService: WeatherService,
              private renderer: Renderer2,
              private toastr: ToastrService) {
    // this.unsubscribe = new Subject<void>();
  }

  ngOnInit() {
    this.cityName = new FormControl('', [
      Validators.required,
      Validators.pattern('[a-zA-Z]+([ ,]+[a-zA-Z]+)*'),
      CitynameValidators.cannotContainAllUppercaseLetters
    ]);

    this.cityForm = new FormGroup({
      cityName: this.cityName
    });
  }

  ngOnDestroy() {
    // this.unsubscribe.next();
    // this.unsubscribe.complete();
    this.errorNames = '';
  }

  ngAfterViewInit() {
    // this.renderer.selectRootElement(this.input['nativeElement']).focus();
  }

  onSubmit(form) {
    this.validateErrorMessages();
    if (!this.cityName.invalid) {
      this.weatherService.getSearchInput(form.value.cityName);
    this.weatherService.searchInput
        .takeUntil(componentDestroyed(this))
        .subscribe(input => {
          this.cities = input.split(',').map(city => city.trim());
          if (this.cities && this.cities.length > 0) {
            this.getErrors(this.cities);
          }
        });
      this.router.navigate(['/search_results']);
    }
  }

  get cityFormName() {
    return this.cityForm.get('cityName');
  }

  validateCityName() {
    return this.cityFormName.invalid && (this.cityFormName.touched || this.mouseoverSearch);
  }

  validateErrorMessages() {
    const form = this.cityForm.controls.cityName.errors;
    if (form && form.required) {
      this.cityName.setErrors({
        isRequired: true
      });
    } else if (form && form.pattern) {
      this.cityName.setErrors({
        invalidPattern: true
      });
    } else if (form && form.cannotContainAllUppercaseLetters) {
      this.cityName.setErrors({
        invalidLetterCases: true
      });
    }
  }

  private getErrors(cityNames: string[]) {
    let errorDuplicatedCityName = '';
    for (let i = 0; i < cityNames.length; i++) {
      this.weatherService.getWeatherByCityName1(cityNames[i]).subscribe(weather => {
        const duplicatedCity = this.getDuplicatedCityName(weather, cityNames[i]);
        if (duplicatedCity) {
          errorDuplicatedCityName += duplicatedCity + ', ';
        }
      }, err => {
        const errorCityName = err.url.split('weather?q=')[1].split('&APPID')[0];
        this.removeCityNameFromCityNamesList(errorCityName);
        this.errorNames += errorCityName + ', ';
      });
    }

    setTimeout(() => {
      if (errorDuplicatedCityName.length > 0) {
        this.displayError(`You already entered: ${this.removeLastCommaInError(errorDuplicatedCityName)}...`);
      }

      if (this.errorNames && this.errorNames.length > 2) {
        this.displayError(`There is no ${this.removeLastCommaInError(this.errorNames)} city...`);
      }
    }, 1000);
  }

  private getDuplicatedCityName(weather: Weather, cityName: string) {
    let errorDuplicatedCityName = '';
    let found = false;
    const weathers = this.weatherService.getWetherItems();

    weathers.forEach(w => {
      if (w.cityName.toLowerCase() === cityName.toLowerCase()) {
        found = true;
        errorDuplicatedCityName = cityName;
      }
    });

    if (!found) {
      this.weatherService.addWeatherItem(weather);
      this.weatherService.addCity(weather.cityName);
    }

    return errorDuplicatedCityName;
  }

  private removeCityNameFromCityNamesList(cityName: string) {
    const deleteIndex = this.cities.indexOf(cityName);
    this.cities.splice(deleteIndex, 1);
  }

  private displayError(message: string) {
    this.toastr.error(message, 'Ooops', {positionClass: 'toast-bottom-left'});
  }

  private removeLastCommaInError(errorNames: string): string {
    errorNames = errorNames.substring(0, errorNames.length - 2);
    return errorNames;
  }

  closeCurrentKeyboard() {
    if (this.keyboardRef) {
      this.keyboardRef.dismiss();
    }

    if (this.enterSubscription) {
      this.enterSubscription.unsubscribe();
    }
  }

  @HostListener('document:click', ['$event'])
  onClickOutside(event) {
    const elementClassName = event.target.className;
    if (elementClassName.indexOf('search-city__body__form__input') !== -1) {
      this.isKeyboard = false;
    } else if ((elementClassName.indexOf('keyboard__input') !== -1 || elementClassName.indexOf('ng-invalid') !== -1)
      || (elementClassName.indexOf('keyboard__input') !== -1 || elementClassName.indexOf('ng-valid') !== -1)) {
      this.isKeyboard = true;
    } else if ((elementClassName !== 'close') && (elementClassName !== 'keyboard__input ng-pristine ng-invalid ng-touched'
      || elementClassName !== 'keyboard__input ng-pristine ng-touched ng-valid')) {
      this.isKeyboard = false;
    }
  }
}
