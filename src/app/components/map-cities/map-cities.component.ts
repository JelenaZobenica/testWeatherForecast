import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Weather} from '../../models/weather.model';
import {ConvertMeasure} from '../../pipes/convert-measure.pipe';
import {FeatureGeoJson} from '../../models/feature-geo-json.model';

declare var google: any;

@Component({
  selector: 'map-cities',
  templateUrl: './map-cities.component.html',
  styleUrls: ['../../../styles/map-cities/map-cities.scss']
})
export class MapCitiesComponent implements OnInit, OnChanges {
  @Input() weathers: Weather[];
  map: any;
  feature: any;
  infoWindow: any;
  geoJSON = {
    type: 'FeatureCollection',
    features: []
  };
  zoomFilteredCity: number;
  zoomAllCities: number;
  initialMapLatitude: number;
  initialMapLongitude: number;

  constructor(private convertMeasure: ConvertMeasure) {
  }

  ngOnInit() {
    this.setInitMapValues();
    setTimeout(() => {
      this.initMap(this.zoomAllCities, this.initialMapLatitude, this.initialMapLongitude);
      this.resetData();
      this.addFeaturesToGeoJSON(this.weathers);
      this.map.data.loadGeoJson(this.geoJSON);
      this.drawIcons(this.geoJSON);
    }, 500);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.weathers.previousValue) {
      const previousWeathers = changes.weathers.previousValue;
      const currentWeathers = changes.weathers.currentValue;
      if (previousWeathers.length !== currentWeathers.length) {
        // const removedCity = changes.weathers.previousValue.filter(x => !changes.weathers.currentValue.includes(x));
        if (previousWeathers.length === 1) {
          this.initMap(this.zoomAllCities, this.initialMapLatitude, this.initialMapLongitude);
        }
        this.resetData();
        this.addFeaturesToGeoJSON(currentWeathers);
        this.drawIcons(this.geoJSON);
      }

      // case if city is filtered
      if (currentWeathers.length === 1) {
        this.initMap(this.zoomFilteredCity, currentWeathers[0].coord.lat, currentWeathers[0].coord.lon);
        this.resetData();
        this.geoJSON.features.push(this.converteWeatherToFeatureGeoJson(currentWeathers[0]));
        this.drawIcons(this.geoJSON);
      }
    }
  }

  setInitMapValues() {
    this.zoomFilteredCity = 7;
    this.zoomAllCities = 2;
    this.initialMapLatitude = 40;
    this.initialMapLongitude = 13;
  }

  initMap(zoom, lat, lon) {
    this.initInfowindow();
    const mapOptions = {
      zoom: zoom,
      center: new google.maps.LatLng(lat, lon)
    };
    this.map = new google.maps.Map(document.getElementById('map-cities'), mapOptions);
    this.map.data.addListener('mouseover', (event) => {
      this.setContentOfInfoWindow(event);
      this.openInfoWindow();
    });
    this.map.data.addListener('mouseout', (event) => {
      this.closeInfoWindow();
    });
  }

  initInfowindow() {
    this.infoWindow = new google.maps.InfoWindow();
  }

  setContentOfInfoWindow(event) {
    const weendSpeed = this.convertMeasure.transform(event.feature.getProperty('windSpeed'));
    this.infoWindow.setContent(
      '<div class="google-info-window" style="text-align: center;">' +
      '<img style="width: 40px; vertical-align: middle" src=' + event.feature.getProperty('icon') + '></div>'
      + '<div style="text-align: center; text-transform: uppercase;font-weight: bold;">'
      + event.feature.getProperty('city') + '</div>'
      + '<div style="text-align: center; font-weight: bold;">'
      + event.feature.getProperty('temperature') + '&deg;C' + '</div>'
      + '<div style="text-align: center; font-weight: bold;">'
      + weendSpeed + 'km/h</div>'
    );
    this.setInfoWindowOptions(event);
  }

  setInfoWindowOptions(event) {
    this.infoWindow.setOptions({
      position: {
        lat: event.latLng.lat(),
        lng: event.latLng.lng()
      },
      pixelOffset: {
        width: 0,
        height: -15
      }
    });
  }

  openInfoWindow() {
    this.infoWindow.open(this.map);
  }

  closeInfoWindow() {
    this.infoWindow.close(this.map);
  }

  converteWeatherToFeatureGeoJson(weatherItem: Weather) {
    const feature = new FeatureGeoJson(weatherItem);

    // Set the custom marker icon
    this.map.data.setStyle((feature) => {
      return {
        icon: {
          url: feature.getProperty('icon'),
          anchor: new google.maps.Point(25, 25)
        }
      };
    });

    return feature;
  }

  addFeaturesToGeoJSON(weathers: Weather[]) {
    for (let i = 0; i < weathers.length; i++) {
      const feature = this.converteWeatherToFeatureGeoJson(weathers[i]);
      this.geoJSON.features.push(feature);
    }
  }

  // Add the markers to the map
  drawIcons(geoJSON) {
    this.map.data.addGeoJson(geoJSON);
  }

  // Clear data layer and geoJSON
  resetData() {
    this.geoJSON = {
      type: 'FeatureCollection',
      features: []
    };
    this.map.data.forEach((feature) => {
      this.map.data.remove(feature);
    });
  }
}
