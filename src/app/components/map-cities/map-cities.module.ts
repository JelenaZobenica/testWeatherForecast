import {NgModule} from '@angular/core';
import {MapCitiesComponent} from './map-cities.component';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {MatIconModule} from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import {MatKeyboardModule} from '@ngx-material-keyboard/core';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ConvertMeasure} from '../../pipes/convert-measure.pipe';

@NgModule({
  declarations: [
    MapCitiesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    MatKeyboardModule,
    MatTooltipModule,
    RouterModule
  ],
  providers: [ConvertMeasure],
  exports: [MapCitiesComponent]
})
export class MapCitiesModule { }
