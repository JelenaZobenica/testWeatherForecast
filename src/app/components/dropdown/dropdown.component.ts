import {Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2} from '@angular/core';
import {Weather} from '../../models/weather.model';
import {WeatherService} from '../../services/weather.service';
import {MatCheckboxChange} from '@angular/material';

@Component({
  selector: 'dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['../../../styles/dropdown/dropdown.scss']
})
export class DropdownComponent implements OnInit {

  @Output() removedCity = new EventEmitter<string>();
  @Input() cities: string[];
  @Input() isOpen: boolean;
  filterValue = '';
  selected = false;
  indeterminate = false;
  deleteCities: string[];

  constructor(private weatherService: WeatherService, private eRef: ElementRef, private renderer: Renderer2) {
  }

  ngOnInit() {
    this.deleteCities = [];
  }

  onClickOutside(event: Object) {
    if (event === true) {
      const part = this.eRef.nativeElement.querySelector('.dropdown');
      this.renderer.setStyle(part, 'display', 'none');
      this.weatherService.clickOutside(true);
      this.deleteCities.forEach(city => {
        this.weatherService.removeCity(city);
        this.removedCity.emit(city);
      });
    }
  }

  /*  @HostListener('document:click', ['$event']) onMouseOut(event) {
   console.log(event.target.className + ' event');
   console.log(this.eRef.nativeElement + ' this.eRef.nativeElement');
   if (event.target.className === 'search-results__nav__items--tag') {
   this.isOpen = true;
   }
   const isClickedInside = this.eRef.nativeElement.contains(event.target);
   console.log(isClickedInside + ' isClickedInside');
   if (!isClickedInside && this.isOpen) {
   let part = this.eRef.nativeElement.querySelector('.dropdown')
   this.renderer.setStyle(part, 'display', 'none');
   }
   }*/

  toggleSelection(change: MatCheckboxChange, cityName: string): void {
    if (change.checked) {
      this.deleteCities.push(cityName);
    } else {
      this.selected = change.checked;
      this.deleteCities = this.deleteCities.filter(city => city !== cityName);
    }
  }

  setDeleteWeathers(weathers: Weather[], selected) {
    // this.deleteWeathers = weathers;
    this.selected = selected;
  }
}
