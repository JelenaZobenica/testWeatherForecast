import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {DropdownComponent} from './dropdown.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {ClickOutsideDirective} from '../../directives/click-outside.directive';
import {MatFormFieldModule, MatInputModule, MatOptionModule} from '@angular/material';
import {DirectiveModule} from '../../directives/directive.module';
import {PipesModule} from '../../pipes/pipe.module';

@NgModule({
  declarations: [
    DropdownComponent,
    ClickOutsideDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatCheckboxModule,
    MatOptionModule,
    MatFormFieldModule,
    MatInputModule,
    DirectiveModule,
    PipesModule
  ],
  providers: [],
  exports: [DropdownComponent]
})
export class DropdownModule {
}
