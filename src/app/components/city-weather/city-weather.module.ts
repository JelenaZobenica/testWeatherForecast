import {NgModule} from '@angular/core';
import {CityWeatherComponent} from './city-weather.component';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {PipesModule} from '../../pipes/pipe.module';

@NgModule({
  declarations: [
    CityWeatherComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    PipesModule
  ],
  providers: [],
  exports: [CityWeatherComponent]
})
export class CityWeatherModule {
}
