import {Component, Input, OnInit} from '@angular/core';
import {Weather} from '../../models/weather.model';
import {ToastrService} from 'ngx-toastr';
import {bounceOutAnimation} from '../../animations/animations';
import {transition, trigger, useAnimation} from '@angular/animations';
import {WeatherService} from '../../services/weather.service';

@Component({
  selector: 'city-weather',
  templateUrl: './city-weather.component.html',
  styleUrls: ['../../../styles/city-weather/city-weather.scss'],
  animations: [
    trigger('weatherItemsAnimation', [
      transition(':leave', [
        useAnimation(bounceOutAnimation)
      ])
    ])
  ]
})
export class CityWeatherComponent implements OnInit {

  @Input() cityWeathers: Weather[];
  date: Date;
  isFadeIn: boolean;
  isSort: boolean;

  constructor(private toastr: ToastrService, private weatherService: WeatherService) {
  }

  ngOnInit() {
    this.date = new Date();
    this.isFadeIn = true;
    this.isSort = false;
    this.weatherService.isFadeIn.subscribe(isFade => this.isFadeIn = isFade);
    this.weatherService.isSorted.subscribe(isSort => this.isSort = isSort);
  }

  addMessage(cityName: string) {
    this.toastr.success(`Daily weather forecast in ${cityName}`, 'Weather forecast', {positionClass: 'toast-top-right'});
  }
}
