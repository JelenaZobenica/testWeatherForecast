import {NgModule} from '@angular/core';
import {CollapsibleForecastComponent} from './collapsible-forecast.component';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    CollapsibleForecastComponent
  ],
  imports: [
    BrowserModule,
    RouterModule
  ],
  providers: [],
  exports: [CollapsibleForecastComponent]
})
export class CollapsibleForecast { }
