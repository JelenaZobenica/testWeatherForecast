import {Component} from '@angular/core';

@Component({
  selector: 'collapsible-forecast',
  templateUrl: './collapsible-forecast.component.html',
  styleUrls: ['../../../styles/collapsible-forecast/collapsible-forecast.scss']
})
export class CollapsibleForecastComponent {

  visible: boolean = false;

  constructor() {}

  toggleContent() {
    this.visible = !this.visible;
  }

  getStartStyle() {
    if (this.visible) {
      return {'text-align' : 'center'};
    }
    return {};
  }
}
