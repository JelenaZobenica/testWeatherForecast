import {Component, HostListener, Input, OnInit} from '@angular/core';
import 'rxjs/add/operator/takeUntil';
import {Weather} from '../../models/weather.model';
import {WeatherService} from '../../services/weather.service';
import {MatDialog} from '@angular/material';
import {DialogBoxComponent} from '../dialog-box/dialog-box.component';

@Component({
  selector: 'search-results-nav-bar',
  templateUrl: './search-results-nav-bar.component.html',
  styleUrls: ['../../../styles/search-results-nav-bar/search-results-nav-bar.scss']
})
export class SearchResultsNavBarComponent implements OnInit {

  @Input() weathers: Weather[];
  isOpen: boolean;
  config = {
    panelClass: 'custom-overlay-pane-class',
    position: {
      top: '250px',
      bottom: '',
      left: '',
      right: ''
    }
  };
  cities: string[];
  currentScreenWidth: number;
  breakpointSmallScreen: number;

  constructor(private weatherService: WeatherService, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.currentScreenWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    this.breakpointSmallScreen = 460;
    this.isOpen = false;
    this.weatherService.isClickedOutside.subscribe(isClicked => this.isOpen = false);
    this.cities = this.weatherService.getCityItems();
    this.weatherService.isRemoved.subscribe(removedCity => this.cities = []);
  }

  removeCity(cityName: string) {
    this.weatherService.removeCity(cityName);
    this.updateCityItems(cityName);
  }

  openDialog() {
    this.dialog.open(DialogBoxComponent, this.config)
      .afterClosed()
      .subscribe(result => {
        if (result === 'yes') {
          this.weathers = [];
          this.weatherService.resetWeatherItems();
          this.weatherService.resetCityItems();
          this.weatherService.removeAllCitiesFromNavBar(true);
        }
      });
  }

  openModal() {
    this.weatherService.openModal(true);
  }

  openDropdown(event) {
    // this.isOpen = event;
    this.isOpen = !this.isOpen;
    event.stopPropagation();
  }

  updateCityItems(cityName: string) {
    this.weatherService.removeCityItems(cityName);
    this.cities = this.weatherService.getCityItems();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.currentScreenWidth = event.target.innerWidth;
    // this.currentScreenWidth = window.innerWidth;
  }
}
