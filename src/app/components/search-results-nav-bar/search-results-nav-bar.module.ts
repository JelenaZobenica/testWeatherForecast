import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {CityWeatherModule} from '../city-weather/city-weather.module';
import {SearchResultsNavBarComponent} from './search-results-nav-bar.component';
import {FirstLetterUpercase} from '../../pipes/first-letter-uppercase.pipe';
import {MatDialogModule} from '@angular/material/dialog';
import {DropdownModule} from '../dropdown/dropdown.module';
import {MatIconModule} from '@angular/material/icon';
import {OpenDropdownDirective} from "../../directives/open-dropdown.directive";

@NgModule({
  declarations: [
    SearchResultsNavBarComponent,
    FirstLetterUpercase,
    OpenDropdownDirective
  ],
  imports: [
    BrowserModule,
    RouterModule,
    CityWeatherModule,
    MatDialogModule,
    DropdownModule,
    MatIconModule
  ],
  providers: [],
  exports: [SearchResultsNavBarComponent]
})
export class SearchResultsNavBarModule { }
