import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {CityWeatherDetailsComponent} from './city-weather-details.component';
import {CollapsibleForecast} from '../collapsible-forecast/collapsible-forecast.module';
import {FormatDate} from '../../pipes/format-date.pipe';
import {NavbarModule} from '../nav-bar/nav-bar.module';
import {
  MatButtonModule, MatDatepickerModule, MatFormFieldModule, MatInputModule,
  MatNativeDateModule
} from '@angular/material';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    CityWeatherDetailsComponent,
    FormatDate
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule,
    CollapsibleForecast,
    NavbarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule
  ],
  providers: [],
  exports: [CityWeatherDetailsComponent]
})
export class CityWeatherDetailsModule {
}
