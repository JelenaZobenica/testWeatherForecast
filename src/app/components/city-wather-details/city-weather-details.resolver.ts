import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {WeatherService} from '../../services/weather.service';
import {Observable} from 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class CityWeatherDetailsResolver implements Resolve<any> {

  constructor(private weatherService: WeatherService) {}

  resolve(route: ActivatedRouteSnapshot): any {
    return this.weatherService.getWeatherByCityName(route.params['name']).catch(() => {
      return Observable.of('data not available at this time');
    });
  }
}
