import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {WeatherService} from '../../services/weather.service';
import {ActivatedRoute} from '@angular/router';
import {Weather} from '../../models/weather.model';
import Chart from 'chart.js';
import * as moment from 'moment';

declare var google: any;

@Component({
  selector: 'city-weather-details',
  templateUrl: './city-weather-details.component.html',
  styleUrls: ['../../../styles/city-weather-details/city-weather-details.scss']
})
export class CityWeatherDetailsComponent implements OnInit, OnDestroy {

  weather: Weather;
  weathers: Weather[];
  today: any;
  choosenDate: string;
  minDate: Date;
  maxDate: Date;
  chart: Chart;
  @ViewChild('canvas') canvas: ElementRef;
  public context: any;
  chartPointIcons: any[];
  metaIndex: any;
//    "start": "node server.js",
  constructor(private weatherService: WeatherService, private route: ActivatedRoute, private elRef: ElementRef) {
    // this.canvas = this.elRef.nativeElement;
  }

  ngOnInit() {
    this.today = new Date().getTime() / 1000;
    this.today = this.formatLongDate(this.today);
    this.weathers = [];
    this.chartPointIcons = [];
    this.route.data.forEach(data => {
      this.weather = data['weather'];

      if (this.weather) {
        this.initMap();
        this.getFiveDayWeatherForecast(this.weather.cityName);
      }
    });
  }

  ngOnDestroy() {
    this.chart.destroy();
    this.chart.config.data.datasets[0]['_meta'] = {};
    this.metaIndex = undefined;
  }

  getFiveDayWeatherForecast(cityName: string) {
    this.weatherService.getFiveDayWeatherByCityName(cityName).subscribe(weathers => {
      this.weathers = weathers;
      if (this.weathers) {
        const year = +this.formatDate(this.weathers[0].date, 'YYYY');
        const month = +this.formatDate(this.weathers[0].date, 'MM') - 1;
        const dayMin = +this.formatDate(this.weathers[0].date, 'D');
        const dayMax = +this.formatDate(this.weathers[this.weathers.length - 1].date, 'D');
        this.minDate = new Date(year, month, dayMin);
        this.maxDate = new Date(year, month, dayMax);
        this.getDataForChart();
      }
    });
  }

  initMap() {
    let map;
    const lat: any = this.weather.coord.lat;
    const lon: any = this.weather.coord.lon;
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: lat, lng: lon},
      zoom: 8
    });

    const marker = new google.maps.Marker({
      position: {lat: lat, lng: lon},
      map: map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      title: this.weather.cityName
    });
  }

  isToday(date: any): boolean {
    date = this.formatLongDate(date);
    const retVal = this.today.split(',')[0] === date.split(',')[0] ? true : false;

    return retVal;
  }

  private formatLongDate(date: any) {
    date = moment.unix(date).format('dddd').substring(0, 3) + ' ' + moment.unix(date).format('MMM Do, h:mm a');
    return date;
  }

  private formatTime(date: any) {
    date = moment.unix(date).format('HH:mm');
    return date;
  }

  private formatTimeMonthAndDay(date: any) {
    date = this.formatTime(date) + ' ' + moment.unix(this.weather.date).format('MMM Do');
    return date;
  }

  onDateChange(event) {
    const date = moment(event.value).unix();
    if (this.weathers.length > 8) {
      this.weathers = this.filterWeather(this.weathers, date);
    } else {
      this.weathers = null;
      this.weatherService.getFiveDayWeatherByCityName(this.weather.cityName).subscribe(weathers => {
        this.weathers = weathers;
        if (this.weathers) {
          this.weathers = this.filterWeather(this.weathers, date);
        }
      });
    }
  }

  formatDate(date, formatPattern) {
    return moment.unix(date).format(formatPattern);
  }

  filterWeather(weathers: Weather[], date) {
    return weathers.filter(w => this.formatDate(w.date, 'MMM Do').indexOf(this.formatDate(date, 'MMM Do')) !== -1);
  }

  resetDatepickerToInitialState() {
    this.choosenDate = undefined;
    this.getFiveDayWeatherForecast(this.weather.cityName);
  }

  private getDataForChart() {
    const allDates = [];
    const averageTempByDays = [];
    let weathersTemp = [];
    const lastWeatherDate = new Date(this.formatDate(this.weathers[this.weathers.length - 1].date, 'YYYY/MM/DD'));
    for (let i = 0; i < this.weathers.length; i++) {
      const currentWeatherDate = new Date(this.formatDate(this.weathers[i].date, 'YYYY/MM/DD'));
      let nextWeatherDate;
      (i + 1) < this.weathers.length ? nextWeatherDate = new Date(this.formatDate(this.weathers[i + 1].date, 'YYYY/MM/DD'))
        : nextWeatherDate = currentWeatherDate;
      weathersTemp.push(this.weathers[i]);
      if (moment(currentWeatherDate).diff(moment(nextWeatherDate), 'days') === -1 ||
        (i === this.weathers.length - 1 && lastWeatherDate.getDay() === nextWeatherDate.getDay())) {
        allDates.push(this.formatDate(weathersTemp[0].date, 'MMM Do'));
        const temp = Math.ceil(this.getSumOfTemperatures(weathersTemp) / weathersTemp.length);
        averageTempByDays.push(temp);
        const indexOfIcon = Math.ceil((weathersTemp.length - 1) / 2);
        this.chartPointIcons.push(weathersTemp[indexOfIcon].weatherIcon);
        weathersTemp = [];
      }
    }
    this.setChart(allDates, averageTempByDays, this.chartPointIcons);
  }

  private getSumOfTemperatures(weathersTemp) {
    const initialValue = 0;
    return weathersTemp.reduce(((sum, value) => sum + value.temperature), initialValue);
  }

  private getMaxTemperature(tempList) {
    return tempList.reduce((a, b) => Math.max(a, b));
  }

  private getMinTemperature(tempList) {
    return tempList.reduce((a, b) => Math.min(a, b));
  }

  private getDatasets(tempData) {
    return {
      label: 'temperature',
      data: tempData,
      backgroundColor: '#06addb',
      borderColor: '#06addb',
      pointBackgroundColor: '#06addb',
      pointRadius: 4,
      fill: false
    };
  }

  private getChartTitleSettings() {
    return {
      display: true,
      text: 'Average daily temperature',
      padding: 25,
      fontSize: 20
    };
  }

  private getTooltipAndHoverSettings(intersect: boolean) {
    return {
      mode: 'nearest',
      intersect: intersect
    };
  }

  private setChart(allDates, tempData, icons) {
    // this.context = (document.getElementById('canvas') as any).getContext('2d');
    this.chart = new Chart('canvas', {
      type: 'line',
      data: {
        labels: allDates,
        datasets: [this.getDatasets(tempData)]
      },
      options: {
        responsive: true,
        title: this.getChartTitleSettings(),
        legend: {
          display: true,
          position: 'bottom'
        },
        tooltips: this.getTooltipAndHoverSettings(false),
        hover: this.getTooltipAndHoverSettings(true),
        scales: {
          xAxes: [{
            display: true,
          }],
          yAxes: [{
            display: true,
            // gridLines: {
            //   display: false
            // },
            // scaleLabel: {
            //   display: true,
            //   labelString: 'Temperature',
            // },
            ticks: {
              min: this.getMinTemperature(tempData) !== 0 ? this.getMinTemperature(tempData) - 1 : 0,
              max: this.getMaxTemperature(tempData) + 1,
              stepSize: 1,
              // beginAtZero: true
              // Include a celsius sign in the ticks
              callback: function (value, index, values) {
                return value + '\u00B0C';
              }
            }
          }]
        }
      }
    });
    this.setChartIcons(icons, this.chart);

    // Chart.pluginService.register({
    //   beforeDatasetDraw: function(chart) {
    //     // chart.config.data.datasets[0]._meta[0].data[4]._model.pointStyle = sun;
    //     chart.config.data.datasets[0]._meta[0].data.forEach((d, index) => {
    //       console.log(icons[index]);
    //         sun.src = icons[index];
    //         d._model.pointStyle = sun;
    //     });
    //   }
    // });
  }

  private setChartIcons(icons, chart) {
    const iconUrls = icons.map(icon => `http://openweathermap.org/img/w/${icon}.png`);
    this.metaIndex = Object.keys(chart.config.data.datasets[0]['_meta']);
    for (let i = 0; i < chart.config.data.datasets[0]._meta[this.metaIndex].data.length; i++) {
      const data = chart.config.data.datasets[0]._meta[this.metaIndex].data[i];
      const image = this.getImage(iconUrls[i]);
      data._model.pointStyle = image;
    }
  }

  private getImage(iconUrl) {
    const image = new Image();
    image.src = iconUrl;
    image.width = 40;
    image.height = 40;
    return image;
  }
}
