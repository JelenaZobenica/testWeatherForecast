import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from "@angular/material";

@Component({
  selector: 'dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['../../../styles/dialog-box/dialog-box.scss']
})
export class DialogBoxComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DialogBoxComponent>) {}

  ngOnInit() {
  }

  closeDialog(confirmation: string) {
    this.dialogRef.close(`${confirmation}`);
  }
}
