import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import {RouterModule} from '@angular/router';
import {DialogBoxComponent} from './dialog-box.component';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [
    DialogBoxComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MatButtonModule,
    RouterModule
  ],
  providers: [],
  exports: [DialogBoxComponent]
})
export class DialogBoxModule { }
