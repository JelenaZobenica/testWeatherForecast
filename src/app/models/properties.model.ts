export class Properties {
  city: string;
  temperature: number;
  min: number;
  max: number;
  humidity: number;
  pressure: number;
  windSpeed: number;
  icon: string;

  constructor(obj?: any) {
    this.city = obj.cityName;
    this.temperature = obj.temperature;
    this.min = obj.minTemp;
    this.max = obj.maxTemp;
    this.humidity = obj.humidity;
    this.pressure = obj.pressure;
    this.windSpeed = obj.windSpeed;
    this.icon = 'http://openweathermap.org/img/w/' + obj.weatherIcon + '.png';
  }
}
