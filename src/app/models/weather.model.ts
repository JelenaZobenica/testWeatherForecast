export class Weather {
  cityName: string;
  description: string;
  temperature: number;
  minTemp: number;
  maxTemp: number;
  humidity: number;
  windSpeed: number;
  cityCountry?: string;
  pressure?: number;
  sunrise?: any;
  sunset?: any;
  coord?: {
    lon: '',
    lat: ''
  };
  date: any;
  weatherIcon: string;

  constructor(obj?: any) {
    this.cityName = obj && obj.name || null;
    this.description = obj && this.makeFirstLetterOfWordUppercase(obj.weather[0].description);
    this.temperature = obj && Math.floor(obj.main.temp);
    this.minTemp = obj && Math.floor(obj.main.temp_min);
    this.maxTemp = obj && Math.floor(obj.main.temp_max);
    this.humidity = obj && obj.main.humidity;
    this.windSpeed = obj && obj.wind.speed;
    this.cityCountry = obj && obj.sys.country || null;
    this.pressure = obj && obj.main.pressure;
    this.sunrise = obj && obj.sys.sunrise || null;
    this.sunset = obj && obj.sys.sunset || null;
    this.coord = obj && obj.coord || null;
    this.date = obj && obj.dt;
    this.weatherIcon = obj && obj.weather[0].icon;
  }

  private makeFirstLetterOfWordUppercase(lowerCaseWord: string) {
    return lowerCaseWord.charAt(0).toUpperCase() + lowerCaseWord.slice(1);
  }
}
