import {Properties} from './properties.model';
export class FeatureGeoJson {
  type: string;
  properties: Properties;
  geometry: any

  constructor(obj?: any) {
    this.type = 'Feature';
    this.properties = new Properties(obj);
    this.geometry = {
      type: 'Point',
      coordinates: [obj.coord.lon, obj.coord.lat]
    };
  }
}
