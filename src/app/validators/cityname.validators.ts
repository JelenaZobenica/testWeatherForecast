import {AbstractControl, ValidationErrors} from '@angular/forms';

export class CitynameValidators {
  static cannotContainAllUppercaseLetters(control: AbstractControl): ValidationErrors | null {
    const cityName = control.value as string;

    if (cityName && cityName.includes(cityName.toUpperCase())) {
      return {cannotContainAllUppercaseLetters: true};
    }

    return null;
  }
}
