import {animate, animation, keyframes, style, transition, trigger, useAnimation} from '@angular/animations';

export let bounceOutAnimation = animation(
  animate('0.7s ease-out', keyframes([
    style({
      offset: .2,
      opacity: 1,
      transform: 'translateX(20px)'
    }),
    style({
      offset: 1,
      opacity: 0,
      transform: 'translateX(-50%)'
    })
  ]))
);

export let bounceOutLeftAnimation = animation(
  animate('1s ease-out', keyframes([
    style({
      offset: .2,
      opacity: 1,
      transform: 'translate3d(20px, 0, 0)'
    }),
    style({
      offset: 1,
      opacity: 0,
      transform: 'translate3d(-2000px, 0, 0)'
    })
  ]))
);

export let fadeInAnimation = animation([
  style({ backgroundColor: '#74ebed', opacity: 0 }),
  // parameters for animate function
  animate('{{ duration }} {{ easing }}')
], {
  // default values for parameters
  params: {
    duration: '1.5s',
    easing: 'ease-out'
  }
});

export let fade = trigger('fade', [
  // state('void', style({ opacity: 0 })),
  // void => * is similar as :enter alias
  transition('void => *', [
    useAnimation(fadeInAnimation)
    // this code is extracted into fadeInAnimation variable
    /*style({ backgroundColor: '#74ebed', opacity: 0 }),
    animate(1500)*/
  ]),
  // * => void is similar as :leave alias
  transition('* => void', [
    useAnimation(bounceOutAnimation)
    // animate(700, style({ opacity: 0 }))
  ])
]);

export let slide = trigger('slide', [
  transition(':enter', [
    style({ transform: 'translateX(-10px)'}),
    animate(700)
  ]),
  transition(':leave', [
    useAnimation(bounceOutAnimation)
    // this code is extracted into bounceOutAnimation variable
    /*animate('0.7s ease-out', keyframes([
      style({
        offset: .2,
        opacity: 1,
        transform: 'translateX(20px)'
      }),
      style({
        offset: 1,
        opacity: 0,
        transform: 'translateX(-100%)'
      })
    ]))*/
  ])
]);
